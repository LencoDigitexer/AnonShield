<p align="center">
  <img src="img/logo48.webp" alt="Logo"></img>

</p>
<h1 align="center">AnonShield</h1> 
<p>Расширение, которое позволит получить <b>Proxy</b> (HTTPS), из свободно размещенных листов. <!--<b><a href="https://github.com/jetkai/proxy-list">Proxy List</a> и <a href="https://github.com/monosans/proxy-list">Proxy List</a></b>.</p>-->
<p>Расширение позволит вам изменить свой адрес или попасть на заблокированный в вашей стране сайт.</p>
<p><b>ВНИМАНИЕ!</b> Мы не несем ответственность за безопасноть этих <b>Proxy</b> (вашей информации, которую вы передадите по этим Proxy) и за совершенные с помощью этого расширения действия.</p>
<p>Мы не советуем передавать чувствительную информацию (пароли, логины, данные кредитных карт и любую другую информацию), пока вы используете <b>Proxy</b>. Также, при неработоспособности Proxy - лучше запросить новый.</p>

<h2>ToDo</h2>
<a href="https://github.com/Erghel/BrokeBedrock/projects/1"><b>ToDo</b></a>

<h2>Пожелания/Исправления</h2>
<a href="https://github.com/Erghel/BrokeBedrock/issues/1"><b>Сюда!</b></a>
